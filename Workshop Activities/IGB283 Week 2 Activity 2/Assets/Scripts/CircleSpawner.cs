﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleSpawner : MonoBehaviour
{
    public float minX = -5;
    public float maxX = 5;

    public float minY = -3;
    public float maxY = 3;

    public int xNumber = 15;
    public int yNumber = 15;

    public GameObject circle;

    // Start is called before the first frame update
    void Start()
    {
        float xDistance = (maxX - minX) / xNumber;
        float yDistance = (maxY - minY) / yNumber;

        for (int j = 0; j < yNumber; j++)
        {
            Vector3 position = new Vector3();
            // Find the yPosition of the row 
            position.y = minY + j * yDistance;
            for (int i = 0; i < xNumber; i++)
            {
                position.x = minX + i * xDistance;
                // Create a new instance of the circle and set its position 
                GameObject circleInstance = Instantiate(circle);
                circleInstance.transform.position = position;
                // Set relivent variables 
                Circle c = circleInstance.GetComponent<Circle>();
                c.minX = this.minX; c.maxX = this.maxX;
                c.minY = this.minY; c.maxY = this.maxY;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
