﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sphere : MonoBehaviour
{
    public Material material;
    public Color color1;
    public Color color2;
    private Mesh mesh;

    // Start is called before the first frame update
    void Start()
    {
        mesh = GetComponent<MeshFilter>().mesh;
        GetComponent<MeshRenderer>().material = material;
    }

    // Update is called once per frame
    void Update()
    {
        SetMeshColor();
    }

    void SetMeshColor()
    {
        Vector3 eyeVector = -Camera.main.transform.forward;
        Vector3[] vertices = mesh.vertices;
        Vector3[] normals = mesh.normals;
        Color[] colors = new Color[normals.Length];

        for (int i = 0; i < normals.Length; i++)
        {
            Vector3 rotatedNormal = transform.rotation * normals[i];
            float dotProduct = Vector3.Dot(eyeVector, rotatedNormal);
            colors[i] = Color.Lerp(color2, color1, dotProduct);
            mesh.colors = colors;
        }
    }
}
