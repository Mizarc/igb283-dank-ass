﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnobChangeScript : MonoBehaviour
{
    public bool isMoving = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        MouseClickAction();
        Move();
        KnobChangeColor();
    }
    void KnobChangeColor()
    {
        if (Input.GetKeyDown("q"))
        {
            GetComponent<SpriteRenderer>().color = Color.white;
        }
        if (Input.GetKeyDown("w"))
        {
            GetComponent<SpriteRenderer>().color = Color.blue;
        }
    }
    void Move()
    {
        if (isMoving == true)
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            this.transform.position = mousePosition;
        }
    }
    void MouseOverAction()
    {
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Collider2D hitCollider = Physics2D.OverlapPoint(mousePosition);
        if (hitCollider && hitCollider.transform.tag == "Knob")
        {
            hitCollider.transform.gameObject.GetComponent<KnobChangeScript>().isMoving = true;
        }
    }
    void MouseClickAction()
    {
        if (Input.GetMouseButtonDown(0))
        {
            MouseOverAction();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            isMoving = false;
        }

    }
}
