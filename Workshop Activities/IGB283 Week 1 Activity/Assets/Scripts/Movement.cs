﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    // Creating a public float variable to be accessed and changed outside of Visual Studio
    public float speed = 5.0f;

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        // Storing the current position to a variable so it can be modified
        Vector3 position = this.transform.position;

        // Moving the object in the x direction
        position.x += speed * Time.deltaTime;

        // Updating the current position
        this.transform.position = position;
    }
}
