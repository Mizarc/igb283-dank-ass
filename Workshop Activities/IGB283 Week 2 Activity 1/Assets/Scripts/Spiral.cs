﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spiral : MonoBehaviour
{
    public float speed = 5.0f;

    public float radius = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        Vector3 position = this.transform.position;

        position.x += speed * Time.deltaTime;

        position.y = Mathf.Sin(position.x) * radius;

        position.z = Mathf.Cos(position.x) * radius;

        this.transform.position = position;
    }
}
