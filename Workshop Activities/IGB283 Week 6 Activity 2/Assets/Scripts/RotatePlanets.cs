﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IGB283;

public class RotatePlanets : MonoBehaviour
{
    public float offset;
    public float speed;
    private Mesh mesh;

    private Mesh tempMesh;
    private IGB283Transform cTransform;

    // Start is called before the first frame update
    void Start()
    {
        cTransform = gameObject.GetComponent<IGB283Transform>();
        mesh = gameObject.GetComponent<MeshFilter>().mesh;    
    }

    // Update is called once per frame
    void Update()
    {
       
        tempMesh = gameObject.GetComponent<MeshFilter>().mesh;

        mesh.Clear();        
        
        mesh = tempMesh;
        

        Vector3[] vertices = mesh.vertices;
        Matrix3x3 T = cTransform.Translate(new Vector3(offset,0,0));
        Matrix3x3 R = cTransform.Rotate(speed * Time.deltaTime);

        Matrix3x3 M = R * T;

        for(int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = M.MultiplyPoint(vertices[i]);
        }

        mesh.vertices = vertices;
        mesh.RecalculateBounds();
    }
}
