﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IGB283;

public class Rectangle : MonoBehaviour
{
    public Mesh mesh;
    public Material material;
    public float angle = 10f;
    private Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.AddComponent<MeshFilter>();
        gameObject.AddComponent<MeshRenderer>();

        mesh = GetComponent<MeshFilter>().mesh;

        GetComponent<MeshRenderer>().material = material;

        mesh.Clear();

        mesh.vertices = new Vector3[]
        {
            new Vector3(0, 0, 1),
            new Vector3(0, 1, 1),
            new Vector3(1, 1, 1),
            new Vector3(1, 0, 1)
        };

        mesh.colors = new Color[] {
            new Color(0.8f, 0.3f, 0.3f, 1.0f),
            new Color(0.8f, 0.3f, 0.3f, 1.0f),
            new Color(0.8f, 0.3f, 0.3f, 1.0f),
            new Color(0.8f, 0.3f, 0.3f, 1.0f)
        };

        mesh.triangles = new int[] { 
            0, 1, 2,
            0, 2, 3
        };

        offset.x = mesh.bounds.size.x / 2;
        offset.y = mesh.bounds.size.y / 2;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3[] vertices = mesh.vertices;

        Matrix3x3 T = Translate(offset);
        Matrix3x3 R = Rotate(angle * Time.deltaTime);
        Matrix3x3 T2 = Translate(-offset);
        Matrix3x3 M = T * R * T2;

        for(int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = M.MultiplyPoint(vertices[i]);
        }

        mesh.vertices = vertices;
        mesh.RecalculateBounds();
    }

    Matrix3x3 Rotate (float angle)
    {
        Matrix3x3 matrix = new Matrix3x3();

        matrix.SetRow(0, new Vector3(Mathf.Cos(angle), -Mathf.Sin(angle), 0.0f));
        matrix.SetRow(1, new Vector3(Mathf.Sin(angle), Mathf.Cos(angle), 0.0f));
        matrix.SetRow(2, new Vector3(0.0f, 0.0f, 1.0f));

        return matrix;
    }

    Matrix3x3 Translate(Vector3 transform){
        Matrix3x3 matrix = new Matrix3x3();

        matrix.SetRow(0, new Vector3(1, 0, transform.x));
        matrix.SetRow(1, new Vector3(0, 1, transform.y));
        matrix.SetRow(2, new Vector3(0, 0, 1));

        return matrix;
    }

    Matrix3x3 Scale(float scale){
        Matrix3x3 matrix = new Matrix3x3();

        matrix.SetRow(0, new Vector3(1 + scale, 0, 0));
        matrix.SetRow(1, new Vector3(0, 1 + scale, 0));
        matrix.SetRow(2, new Vector3(0, 0, 1));

        return matrix;
    }
}