﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IGB283;

public class Triangle : MonoBehaviour
{
    public Material material;

    public int x;
    public int y;
    public int z;

    public Mesh mesh;

    public IGB283Transform customTransform;

    public float angle = 10f;

    // Start is called before the first frame update
    void Start()
    {   
        customTransform = gameObject.GetComponent<IGB283Transform>();

        gameObject.AddComponent<MeshFilter>();
        gameObject.AddComponent<MeshRenderer>();

        mesh = GetComponent<MeshFilter>().mesh;

        GetComponent<MeshRenderer>().material = material;

        mesh.Clear();

        mesh.vertices = new Vector3[] {
            new Vector3(-0.29f,-0.29f),
            new Vector3(0.29f,-0.29f),
            new Vector3(0.50f,-0.14f),
            new Vector3(0.43f,-0.14f),
            new Vector3(0.14f,-0.14f),
            new Vector3(0.00f,0.00f),
            new Vector3(0.43f,0.00f),
            new Vector3(0.43f,0.14f),
            new Vector3(0.36f,0.14f),
            new Vector3(0.14f,0.14f),
            new Vector3(0.29f,0.29f),
            new Vector3(0.14f,0.29f),
            new Vector3(0.14f,1.00f),
            new Vector3(0.00f,0.86f),
            new Vector3(0.00f,0.29f),
            new Vector3(-0.07f,0.29f),
            new Vector3(-0.07f,1.00f),
            new Vector3(-0.21f,0.86f),
            new Vector3(-0.21f,0.29f),
            new Vector3(-0.29f,0.29f),
            new Vector3(-0.43f,0.71f),
            new Vector3(-0.43f,0.86f),
            new Vector3(-0.57f,0.71f),
            new Vector3(-0.43f,0.29f),
            new Vector3(-0.43f,0.14f),
        };

        mesh.colors = new Color[] {
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
        };

        mesh.triangles = new int[]{
            0,1,5,
            4,2,1,
            3,6,2,
            8,7,6,
            9,10,7,
            0,19,10,
            14,12,11,
            14,13,12,
            18,16,15,
            18,17,16,
            0,24,19,
            24,20,19,
            23,22,21,
        };
    }

    // Update is called once per frame
    void Update()
    {
        // Get the vertices from the matrix
        Vector3[] vertices = mesh.vertices;
        
        // Get the rotation matrix
        Matrix3x3 R = customTransform.Rotate(angle * 0);

        Matrix3x3 T = customTransform.Translation(new Vector3(x, y, z));

        Matrix3x3 S = customTransform.Scale(0.0f);

        Matrix3x3 M = S * R * T;

        // Rotate each point in the mesh to its new position
        for(int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = M.MultiplyPoint(vertices[i]);
            //vertices[i] = S(vertices[i]) * T(vertices[i]) * R(vertices[i]);
        }

        // Set the vertices in the mesh to their new position
        mesh.vertices = vertices;

        // Recalculate the bounding volume
        mesh.RecalculateBounds();
        


    }
    
}
