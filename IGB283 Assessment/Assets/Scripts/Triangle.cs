﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IGB283;

public class Triangle : MonoBehaviour
{
    public Material material;

    public float x;
    public float y;
    public float z;

    public Vector3 center;
    public Vector3 offset;
    public float time;
    public Mesh mesh;

    public Vector3 moveDirection;

    public IGB283Transform customTransform;

    public float angle = 5f;
    public float rotateSpeed = 10f;

    public float translateX;
    public float translateSpeed;

    public float translateY;

    public bool isLeft = false;

    public int bounds = 7;

    public float scale;
    public float scaleSpeed;

    // Start is called before the first frame update
    void Start()
    {   
        gameObject.AddComponent<MeshFilter>();
        gameObject.AddComponent<MeshRenderer>();

        customTransform = gameObject.GetComponent<IGB283Transform>();
        ResetMesh();
    }

    void ResetMesh () {

        mesh = GetComponent<MeshFilter>().mesh;
        
        GetComponent<MeshRenderer>().material = material;

        mesh.Clear();

        mesh.vertices = new Vector3[] {
            new Vector3(-0.29f,-0.29f),
            new Vector3(0.29f,-0.29f),
            new Vector3(0.50f,-0.14f),
            new Vector3(0.43f,-0.14f),
            new Vector3(0.14f,-0.14f),
            new Vector3(0.00f,0.00f),
            new Vector3(0.43f,0.00f),
            new Vector3(0.43f,0.14f),
            new Vector3(0.36f,0.14f),
            new Vector3(0.14f,0.14f),
            new Vector3(0.29f,0.29f),
            new Vector3(0.14f,0.29f),
            new Vector3(0.14f,1.00f),
            new Vector3(0.00f,0.86f),
            new Vector3(0.00f,0.29f),
            new Vector3(-0.07f,0.29f),
            new Vector3(-0.07f,1.00f),
            new Vector3(-0.21f,0.86f),
            new Vector3(-0.21f,0.29f),
            new Vector3(-0.29f,0.29f),
            new Vector3(-0.43f,0.71f),
            new Vector3(-0.43f,0.86f),
            new Vector3(-0.57f,0.71f),
            new Vector3(-0.43f,0.29f),
            new Vector3(-0.43f,0.14f),
        };

        mesh.colors = new Color[] {
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7), 0.92f, 0.016f, 1.0f),
            new Color(1.0f * (translateX/7)`, 0.92f, 0.016f, 1.0f),
        };

        mesh.triangles = new int[]{
            0,1,5,
            4,2,1,
            3,6,2,
            8,7,6,
            9,10,7,
            0,19,10,
            14,12,11,
            14,13,12,
            18,16,15,
            18,17,16,
            0,24,19,
            24,20,19,
            23,22,21,
        };

    }

    // Update is called once per frame
    void Update()
    {
        ResetMesh();
        
        angle += rotateSpeed * Time.deltaTime;

        if (translateX > bounds)
        {
            isLeft = true;
        }
        else if (translateX < -bounds)
        {
            isLeft = false;
        }

        if (isLeft){
            translateX -= Time.deltaTime * translateSpeed;
            scale -= scaleSpeed * Time.deltaTime;
        }
        else{
            translateX += Time.deltaTime * translateSpeed;
            scale += scaleSpeed * Time.deltaTime;
        }

        // Get the vertices from the matrix
        Vector3[] vertices = mesh.vertices;
        
        // Get the rotation matrix
        Matrix3x3 R = customTransform.Rotate(angle);
        Matrix3x3 T = customTransform.Translation(new Vector3(translateX,translateY,z));       
        Matrix3x3 S = customTransform.Scale(scale);

        Matrix3x3 M = T * R * S;

        // Rotate each point in the mesh to its new position
        for(int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = M.MultiplyPoint(vertices[i]);
            
        }

        // Set the vertices in the mesh to their new position
        mesh.vertices = vertices;

        // Recalculate the bounding volume
        mesh.RecalculateBounds();

    }
    
}
