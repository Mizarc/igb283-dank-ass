﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Triangle : MonoBehaviour
{
    public Material material;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.AddComponent<MeshFilter>();
        gameObject.AddComponent<MeshRenderer>();

        Mesh mesh = GetComponent<MeshFilter>().mesh;

        GetComponent<MeshRenderer>().material = material;

        mesh.Clear();

        mesh.vertices = new Vector3[]
        {
            new Vector3(-4,-4),
            new Vector3(4,-4),
            new Vector3(7,-2),
            new Vector3(6,-2),
            new Vector3(2,-2),
            new Vector3(0,0),
            new Vector3(6,0),
            new Vector3(6,2),
            new Vector3(5,2),
            new Vector3(2,2),
            new Vector3(4,4),
            new Vector3(2,4),
            new Vector3(2,14),
            new Vector3(0,12),
            new Vector3(0,4),
            new Vector3(-1,4),
            new Vector3(-1,14),
            new Vector3(-3,12),
            new Vector3(-3,4),
            new Vector3(-4,4),
            new Vector3(-6,10),
            new Vector3(-6,12),
            new Vector3(-8,10),
            new Vector3(-6,4),
            new Vector3(-6,2),
        };

        mesh.colors = new Color[] {
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
            new Color(1.0f, 0.92f, 0.016f, 1.0f),
        };

        //mesh.colors = new Color[25];
        //for (int i = 0; i < mesh.colors.Length; i+= 1)
        //{
        //    mesh.colors[i] = new Color(0.8f, 0.3f, 0.3f, 1.0f);
        //}

        mesh.triangles = new int[] { 0, 1, 5, 0, 10, 19 , 1 , 2 , 4 , 2 , 3 , 6, 8 , 7 , 6, 10 , 9 , 7, 11, 12, 14, 12, 14, 13, 16, 15, 18, 16, 17, 18, 19, 0 , 24, 19, 24, 20, 23, 21, 22};

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Transform(Matrix4x4 transform)
    {
        Vector3[] newVertices = 
    }
}
